# Masterpiece

### Tech stack
* HTML/CSS
* Grid layout

### Demo
Live demo is hosted [here](https://kit-portfolio.gitlab.io/simple-layouts/masterpiece/).

### Launch
To run the project follow the next steps:
* No magic here - simple HTML project

# Preview
![Project preview](preview.png#center)